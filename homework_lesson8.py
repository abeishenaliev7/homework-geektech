# Напишите приммеры использования всех операций со словарями
    # clear()

a = {'name' : 'Azamat',
     'age' : 23}
a.clear()
print(a)
print()


    # copy()

dict = {
        'name' : 'Urmat',
        'surmane' : 'Jumabekov',
        'age' : 12
}
new = dict.copy()
print(new)
print()


    # fromkeys()
c = {
    'aza' : 'name',
    19 : 'age'
    }

keys = new.fromkeys(new)
print(keys)


key = dict.fromkeys(dict, c)
print(key)
print()

    # get()

men = {'name': 'Cristiano', 'age': 36, 'profession' : 'footballer'}

print('Name: ', men.get('name'))
print('profession:', men.get('profession'))
print()


    # items()

print(men.items())

newmen = men.items()
print(newmen)
print()


    # keys()

keyy = men.keys()
print(keyy)
print()

    # values()

value = men.values()
print(value)
print()

    # pop()

p = men.pop('name')
print(p)
print()


    # popitem()


player = {'name': 'Ozil', 'age': 32, 'profession' : 'footballer'}
f = player.popitem()
print(f)
print('player =', men)
print()


# Оберните все операции в функции, которые принимают словарь и выполняют над ним операцию. Функцию надо вызвать.
    # clear()

RealMadrid = {
    'captain' : 'Ramos',
    'forward' : 'Benzema'
}
def football(footballer):
    footballer.clear()
    print(footballer)

football(RealMadrid)
print()

    # copy()

Juventus = {
    'captain' : 'Chiellini',
    'legend' : 'Buffon'
}

Juve2= Juventus

def palyer():
    print(Juventus)
    print(Juve2)

palyer()
print()


    # fromkeys()

def play(klk):
    print(klk.fromkeys(Juventus))
play(Juventus)
print()

    # get()

def player(i):
    print(i.get('captain'))
player(Juventus)
print()

    # items()

def igroki(l):
    print(l.items())
igroki(Juventus)
print()
    # keys()

def pl(j):
    print(j.keys())
pl(Juventus)
print()

    # values()

def foot(h):
    print(h.values())
foot(Juve2)
print()

    # pop()

def foo(e):
    print(e.pop('legend'))
foo(Juve2)
print()


# popitem()

def football(b):
    print(b.popitem())
football(Juve2)
print()


# Задача для гугления и самостоятельной рабооты. Разобрраться как работает метод dict.update() и dict.setdefault()
# --- > Здесь опишите решение

name = {
    'name': 'azamat',
    'age': 25,
    'gendare': 'male'
}

adress = {
    'city' : 'Biishkek'
}

name.update(adress)
print(name)

men = name.setdefault('city')
print('person=', name)
print('city =', men)
print()

# Напишите пример вложенной функции.
# --- > Здесь опишите решение

kyzdar = {'name': 'Aidai', 'age': 21}

def women(kyz):
    def womens(i):
        print(i)
    womens(kyz)
women(kyzdar)
print()


# Напишите функцию принимающую массив (состоящий из слов, название файла) и при помощи данных аргументов создающую запись в файле

files = ['s1.txt', 's2.txt', 's3.txt']


def open_files(filesName):
    for fileName in filesName:
        file = open(fileName, 'w')
        file.write('Hello World')
        file.close()


open_files(files)


# Напишите функцию принимающую массив (состоящий из слов, название файла) и при помощи данных аргументов дополняющую файл новыми записями

files2 = ['tst1.txt', 'tst2.txt', 'tst3.txt']
def files(filename):
    for fil in filename:
        file = open(fil, 'a')
        file.write('Hello Azamat')
        file.close()
files(files2)


# Напишите функцию считывающую данные из файла



# Напишите функцию записи в файл которая приниммает в себя данные, отфильтровывает их и записывает только отфильтрованные данные

mens = [
    {
        'name' : 'Bektur',
        'age' : 19
    },
    {
        'name' : 'Adilet',
        'age' : 23
    },
    {
        'name' : 'Urmat',
        'age' : 25
    }
]

def parni(t):
    file = open("test.txt", "w")
    for man in t:
        if man['age'] >= 20:
            print('prohodit', man['name'])
            file.write(f"Name: {man['name']}\nAge:{man['age']}\n")
    file.close()


parni(mens)

