# Вставляйте решение под задачей
# К просмотре обязательно ---- https://www.youtube.com/watch?v=zZBiln_2FhM
# Создайте массив состоящий из 5ти словарей
# --- > Здесь опишите решение
carsmodel = [
    {
        'model': 'Mercedes',
        'series': 'S500',
        'color': 'white',
        'year': 2003
    },
    {
        'model': 'Mercedes',
        'series': 'E430',
        'color': 'black',
        'year': 2002
    },
    {
        'model': 'Bmw',
        'series': '530',
        'color': 'red',
        'year': 2008
    },
    {
        'model': 'Lexus',
        'series': 'Gs-300',
        'color': 'green',
        'year': 2005
    },
    {
        'model': 'Toyota',
        'series': 'Camry',
        'color': 'blue',
        'year': 2007
    }]
print(carsmodel[3])


# Создайте массив состоящий из 5ти массивов с любыми значениями
# --- > Здесь опишите решение
arr = [10, 20, ['windows', 'ios', 'linux'], [1, 2, 3, 4], ['pepsi', 'cola', 'sprite'], ['honda', 'toyota'], ['samsung', 'apple', 'huawei']]


# Замените в ранее созданном массиве несколько значений
# --- > Здесь опишите решение
my_list = [10, 20, ['windows', 'ios', 'linux'], [1, 2, 3, 4], ['pepsi', 'cola', 'sprite'], ['honda', 'toyota'], ['samsung', 'apple', 'huawei']]
my_list[2] = ['ubuntu']
my_list[3] = [15, 198, 5645]
my_list[4] = ['mirinda', 'tan', 'shoro']
print(my_list)


# Создайте цикл для первого масссива
# --- > Здесь опишите решение
for ggg in arr[2]:
    print(ggg)


# Создайте цикл для второго масссива
# --- > Здесь опишите решение
for lll in arr[3]:
    print(lll)


# Создайте массив состоящий из 10 цифр от 0 и до 20
# --- > Здесь опишите решение
power = (4, 8, 18, 19, 15, 2, 1, 20, 14, 16)


# Пройдитесь циклоом по вышесозданному муссиву и выведите все чита которые больше 10ти
# --- > Здесь опишите решение
for klk in power:
    if klk > 10:
        print(klk)


# Создайте словарь который содержит в себе все типы данных которые мы проходили (ключи на ваше усморение)
# --- > Здесь опишите решение
dannye = {
    'name': 'Azamat',
    'age': 23,
    'born in Bishkek': True,
    'family': ['father', 'mother', 'sister', 'brother'],
    'rost i ves': {
        'rost': 175,
        'ves': 90
    }
}
print(dannye)


# Создайте словарь который содержит в себе 5 разных массивов
# --- > Здесь опишите решение

slovar = {
    'cars': ['bmw', 'mrcedes', 'lexus'],
    'laptop': ['hp', 'lenovo', 'acer'],
    'phone': ['iphone', 'huawei', 'samsung'],
    'water': ['bonaqua', 'legenda', 'issyk-ata'],
    'chocolate': ['twix', 'snickers', 'kitkat']
}
for ghg in slovar['cars']:
    print(ghg)


# Создайте условия if / else
# --- > Здесь опишите решение
domashneezadanie = 'sdelal'
if domashneezadanie == 'ne_sdelal':
    print('--postavit-5--')
else:
    print('--postavit-2--')


# Создайте условия if / elif / else
# --- > Здесь опишите решение
Azat = {
    'age': 15,
    'rost': 165,
    'ves': 50

}
if Azat['age'] >= 18:
    print('--prohodit--')
elif Azat['rost']>= 160:
    print('--prohodit--')
else:
    print('--ne prohodit--')


# Создайте условия if / elif / elif / else
# --- > Здесь опишите решение
kairat = {
    'Age': 20
}
if kairat['Age'] >= 21:
    print('--on zakonchil univer--')
elif kairat['Age'] <= 17:
    print('--on uchitsya v shkole--')
elif kairat['Age'] >= 17:
    print('--on uchitsya v univere--')
else:
    print('--on bezdelnik--')


# Создайте условия цикл который проходится по массиву в который содержит в себе условия if / elif / else
# --- > Здесь опишите решение

car= ['Mercedes', 'bmw', 'lexus', 'toyota', 'kia']
for hp in car:
    if hp == 'Mercedes':
        print('--The best or nothing')
    elif hp == 'lexus':
        print('--Mojno ezdit--')
    else:
        print('--The bad--')