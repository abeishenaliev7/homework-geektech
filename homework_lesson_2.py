# Вставляйте решение под задачей

# Создайте массив состоящий из 3х цифр
# --- > Здесь опишите решение

A = [10, 20, 30]


# Создайте массив состоящий из 5ти строк
# --- > Здесь опишите решение

B = ['Arsenal', 'Liverpool', 'Chelsea', 'Tottenham', 'MU']


# Создайте массив из строк и чисел
# --- > Здесь опишите решение

C = [1, 2, 3, 'Iphone', 'Huawei', 'Samsung', 4, 'Nokia', 5]


# Создайте 2 массив (1ый состоящий из строк, 2ой из чисел) объедените массивы в один
# --- > Здесь опишите решение

D = [10, 11, 12, 13 ,14]
C = ['eleven', 'twelve', 'thirteen', 'fourteen']
print(D+C)


# Создайте массив с вложенными массивами
# --- > Здесь опишите решение

E = [1, 2, 3, 4, ['one', 'two', 'three', 'four', 'five'], 5]


# Создайте массив с несколькими вложенными массивами
# --- > Здесь опишите решение

F = [20, 21, 22, ['Bmw', 'Meredes', 'Audi', [1, 2, 3, 4, [11, 12, 13]], 'Lexus'], 23]


# Достаньте имя значение Ayzhana из нижеприведенного массива
first = [0, 1, 2, 3, 4, [1, 2, 3, [5, 6, 7, [1, 2, 'Ayzhana', 3]], 4]]
# --- > Здесь опишите решение

print(first[5][3][3][2])


# Достаньте имя значение Aselya из нижеприведенного массива
second = [0, 1, 2, 3, 4, [1, 2, 3, [5, 6, 7, [1, 2, 3], [['Aselya']]], 4]]
# --- > Здесь опишите решение

print(second[5][3][4][0][0])


# Выведите произведение каждого числа на "7" нижеприведенного массива используя цикл
third = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# --- > Здесь опишите решение

for aza in third:
    result = aza * 7
    print(result)

# Выведите сумму каждого числа нижеприведенного массива в числом "5" используя цикл
fourth = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# --- > Здесь опишите решение

for az in fourth:
    result = az + 5
    print(result)


# Напишите несколько примеров с использованием конструкции "+="
# --- > Здесь опишите решение

Num1 = [10, 20, 30, 40, 50, 60]

for azamat in Num1:
    azamat += 7
    print(azamat)


# Напишите несколько примеров с использованием конструкции "-="
# --- > Здесь опишите решение

Num2 = [5, 10, 15, 20, 25, 30]

for ggg in Num2:
    ggg -= 5
    print(ggg)


# Напишите несколько примеров с использованием конструкции "*="
# --- > Здесь опишите решение

Num3 = [2, 4, 6, 8, 10]

for aaa in Num3:
    aaa *= 2
    print(aaa)


# Выведите произведение каждого числа на само число нижеприведенного массива используя цикл
fifth = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# --- > Здесь опишите решение

for c in fifth:
    c *= c
    print(c)


# Выведите каждое имя сумированноое со строкой " - Marmeladze", используйте цикл
sixth = ['Ayzhana', 'Zhabeka', 'Ayana', 'Aselya', 'Ademi', 'Maysalbek']
# --- > Здесь опишите решение

for k in sixth:
    k += ' - Marmeladze'
    print(k)


# Cоздайте сложный пример используя все математические операторы ( + - / *)
# Напиромер (2 + 1) * (10 - 4) / 2
# --- > Здесь опишите решение

Mod = [100, 200, 300, 400, 500]
for jjj in Mod:
    jjj = jjj * (3 - 1) + (jjj / 10)
    print(jjj)


# Выведите все элементы вложенного массива при помощи цикла
fifth = [1, 2, 3, 4, 5, 6, [0, 8, 6, 4, 3, 2, 1], 7, 8, 9, 10]
# --- > Здесь опишите решение

for klk in fifth:
    print(klk)



# Выведите все элементы самого глубоко вложенного массива при помощи цикла
fifth = [1, 2, 3, 4, 5, 6, [0, 8, 6, 4, [2, 3, 5], 3, 2, 1], 7, 8, 9, 10]
# --- > Здесь опишите решение

for tmt in fifth:
    print(fifth[6][4])




