# Напишите функцию которая принемает *args

people1 = [
    {
        'name': 'Sergio Ramos',
        'age': 33,
        'national': 'Spain',
        'profession': 'footballer'
    },
    {
        'name': 'Justin Bieber',
        'age': 25,
        'national': 'USA',
        'profession': 'singer'
    }
]

people2 = [
    {
        'name': 'Enrique Iglesias',
        'age': 50,
        'national': 'Spain',
        'profession': 'singer'
    },
    {
        'name': 'Leonardo Di Caprio',
        'age': 45,
        'national': 'USA',
        'profession': 'acter'
    }
]

people3 = [
    {
        'name': 'Kiliam Mbappe',
        'age': 21,
        'national': 'France',
        'profession': 'footballer'
    },
    {
        'name': 'Mirbek Atabekov',
        'age': 33,
        'national': 'Kyrgyzstan',
        'profession': 'singer'
    }
]

def znamenitosti(*args):
    for people in args:
        for singer in people:
            if singer ['profession'] == 'singer':
                print('--the best vocal--', singer)


znamenitosti(people1, people2, people3)
print()


# Напишите функцию которая принемает *kwargs


def information(**kwargs):
    print(kwargs)
information(name='Azamat', surname='Beishenaliev',age=23)
print()

# Напишите функцию которая принемает *args и проходит циклом по пришедшмим данным

a = ['Azamat', 'Bektur', 'Ulukbek']
def mens(*args):
    for imena in args:
        for parni in imena:
            print(parni)
mens(a)
print()


# Напишите имитацию работу клуба (массив девушек, массив парней, охраник, администратор), но с использованием параметров args, kwargs

girls = [
    {
        'name' : 'Sabina',
        'age': 19
    },
    {
        'name' : 'Karina',
        'age': 25
    },
    {
        'name' : 'Adelya',
        'age' : 18
    }
]

mens = [
    {
        'name' : 'Aibek',
        'age': 22
    },
    {
        'name' : 'Nurs',
        'age' : 17
    },
    {
        'name' : 'Kyken',
        'age' : 32
    }
]

def people(*args, **kwargs):
    for peop in args:
        for molodej in peop:
            if molodej['age'] >= 19:
                print(kwargs['control'] + ' : ' + molodej['name'] + ' vy prohodite. ' + kwargs['hostes'] + ' postavila pechat\', dobro pojalovat\' v AVE')
            else:
                print(kwargs['control']+ ' : ' + molodej['name'] + ' izvinite v drugoi raz my vas propustim ')



people(girls, mens, control='Aibek-ohranik', hostes='Aselya-administrator')









