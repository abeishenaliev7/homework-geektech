
# Написать функцию которая принимает числа, выводит сумму чисел. Функцию надо вызвать.
num1 = (10, 20, 30, 40, 50, 60, 70, 80, 90, 100)
def print_massiv(maasiv):
    for i in maasiv:
        i += 5
        print(i)

print_massiv(num1)
print()


# Написать функцию которая принимает числа, выводит разность чисел. Функцию надо вызвать.

def print_massiv(maasiv):
    for i in maasiv:
        i -= 10
        print(i)
print_massiv(num1)
print()


# Написать функцию которая принимает числа, выводит произведение чисел. Функцию надо вызвать.

def print_massiv(maasiv):
    for i in maasiv:
        i *= 2
        print(i)
print_massiv(num1)
print()


# Написать функцию которая принимает числа, выводит деление чисел. Функцию надо вызвать.

def print_massiv(maasiv):
    for i in maasiv:
        i /= 2
        print(i)
print_massiv(num1)
print()

# Написать функцию которая принемает массив, проходится по циклом по массиву и печатает объекты массива. Функцию надо вызвать.

arr = [1, 2, 3, 'Azamat', 'Urmat', [10, 20, 30, 40, 50], 'Ermek']

def massiv(imena):
    for g in imena:
        print(g)
massiv(arr)
print()
massiv(arr[5])
print()


# Напишите приммеры использования всех операций с массивами


# len()
mayarr = ['azamat', 'ulukbek', 'urmat', 10, 20, 10]
print(len(mayarr))
print()


# append()
mayarr.append('new york')
print(mayarr)
print()


# clear()
mayarr.clear()
print(mayarr)
print()


myarr = [10, 10, 20, 30, 40, 50, 60]


# count()
print(myarr.count(10))
print()


# copy()


myarr2 = myarr.copy()
print(myarr2)
print()


# extend()

myarr2.extend(arr)
print(myarr2)
print()


# index()

print(myarr2.index('Azamat'))
print()


# insert
myarr2.insert(2, 'Malik')
print(myarr2)
print()


# remove('Meder')

myarr2.remove('Urmat')
print(myarr2)
print()

# reverse()

myarr2.reverse()
print(myarr2)
print()

# pop()

myarr2.pop(1)
print(myarr2)
print()


# Оберните все оерации в функции, которые принимают масссив и выполняют над нимм операцию. Функцию надо вызвать.

# len()
# append()
c = ['mercedes', 'bmw', 'audi', 'bentley']
def autosalon(auto):
    auto.append('rolls-royce')
    print(auto)

    car = len(auto)
    print(car)

autosalon(c)
print()

# clear()

c1 = ['mercedes', 'bmw', 'audi', 'bentley']

def autosalon(auto):
    auto.clear()
    print(auto)

autosalon(c1)
print()

# count()

c2 = [1, 2, 5, 6, 5, 55, 6]
def number():
    print(c2.count(5))
number()
print()

# copy()

arr1 = [1, 2, 3, 4, 77, 88, 55]
arr2 = arr1.copy
def number():
    print(arr1)
    print(arr2)
number()
print()


# index()

def number():
    print(arr1.index(77))
number()
print()


# remove()
imena = ['Nurlan', 'Aidai', 'Nurbek', 'Kairat']

imena.remove('Aidai')
def imya():
    print(imena)
imya()
print()


# reverse()

imena.reverse()
def imya():
    print(imena)
imya()
print()


# pop()


def imya():
    imena.pop()
    print(imena)
imya()
